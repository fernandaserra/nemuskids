#ifndef NeuralNetwork_hpp
#define NeuralNetwork_hpp

#include <stdio.h>
#include <vector>
#include "NeuronLayer.hpp"
using namespace std;

class NeuralNetwork
{
    public :
        double learning_rate = 0.5;
        int num_inputs;
        NeuronLayer hidden_layer, output_layer;
        NeuralNetwork(int num_inputs, int num_hidden, int num_outputs, vector<double> hidden_layer_weights, double hidden_layer_bias, vector<double> output_layer_weights, double output_layer_bias);
        void init_weights_from_inputs_to_hidden_layer_neurons(vector<double> hidden_layer_weights);
        void init_weights_from_hidden_layer_neurons_to_output_layer_neurons(vector<double> output_layer_weights);
        void inspect();
        vector<double> feed_forward(std::vector<double> inputs);
        void train(vector<double> training_inputs, vector<double> training_outputs);
        double calculate_total_error(vector<vector<double> > training_sets);

};

#endif

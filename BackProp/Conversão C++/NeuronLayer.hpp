#ifndef NeuronLayer_hpp
#define NeuronLayer_hpp

#include <stdio.h>
#include <vector>
#include "Neuron.hpp"
using namespace std;

class NeuronLayer
{
    public:
        double bias;
        vector<Neuron> neurons;
        NeuronLayer(int num_neurons, double bias);
        NeuronLayer();
        void criaNeuronios(int num_neurons);
        void setBias(double bias);
        void inspect();
        vector<double> feed_forward(vector<double> inputs);
        vector<double> get_outputs();
};

#endif

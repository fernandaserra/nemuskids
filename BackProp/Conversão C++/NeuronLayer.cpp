#include "NeuronLayer.hpp"
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

NeuronLayer :: NeuronLayer()
{

}

void NeuronLayer :: criaNeuronios(int num_neurons)
{
    neurons = vector<Neuron>();
    for (int i = 0; i < num_neurons; i++)
    {
        neurons.push_back(Neuron(this->bias));
    }
}

void NeuronLayer :: setBias(double bias)
{
    this->bias = bias;
}

NeuronLayer :: NeuronLayer(int num_neurons, double bias)
{
    if (!bias)
    {
        srand((unsigned int)time(NULL));
        this->bias = (double) ((rand()%10)/10);
    }else
    {
        this->bias = bias;
    }
    neurons = vector<Neuron>();
    for (int i = 0; i < num_neurons; i++)
    {
        neurons.push_back(Neuron(this->bias));
    }
}

void NeuronLayer :: inspect()
{
    cout << "Neurons: " << neurons.size() << "\n";
    for (int n = 0; n < neurons.size(); n++)
    {
        cout << "Neuron " << n << "\n";
        for (int w = 0; w < neurons[n].weights.size(); w++)
        {
            cout << " Weight: " << neurons[n].weights[w];
        }
        cout << "  Bias: " << bias << "\n";
    }
}

vector<double> NeuronLayer :: feed_forward(vector<double> inputs)
{
    vector<double> outputs = vector<double>();
    int i = 0;
    for (Neuron neuron : neurons)
    {
        neurons[i].output = neuron.calculate_output(inputs);
        neurons[i].inputs = inputs;
        outputs.push_back(neurons[i].output);
        i++;
    }
    return outputs;
}

vector<double> NeuronLayer :: get_outputs()
{
    vector<double> outputs = vector<double>();
    for (Neuron neuron : neurons)
    {
        outputs.push_back(neuron.output);
    }
    return outputs;
}

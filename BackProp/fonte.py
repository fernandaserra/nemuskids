import random
import math

class Neuron: # Classe dos neurônios
    def __init__(self, bias): # Construtor
        self.bias = bias # Inicializa o bias com o valor que foi passado pelo construtor
        self.weights = [] # Inicializa o vetor de pesos, vazio inicialmente.

    def calculate_output(self, inputs): # Método que calcula a saída do neurônio a partir das entradas
                                        # passadas por parâmetro através do vetor 'inputs'
        self.inputs = inputs # O vetor de 'inputs' recebe os valores passados por parâmetro
        self.output = self.squash(self.calculate_total_net_input()) # O vetor de outputs recebe as saídas
                                                                    # do neurônio após ter gerado a entrada
                                                                    # total líquida e compactada
        return self.output # Retorna a saída do neurônio

    def calculate_total_net_input(self): # Calcula a entrada total líquida do neurônio dadas todas as entradas juntas
        total = 0 # Cria acumulador
        for i in range(len(self.inputs)): # Repete i vezes, sendo i o número total de entradas
            total += self.inputs[i] * self.weights[i] # Acumula a soma das entradas multiplicadas por seus respectivos pesos
        return total + self.bias # Retorna o acumulador do valor total mais o bias

    def squash(self, total_net_input): # Método que aplica a função logística para compactar/esmagar a saída do neurônio,
                                       #feito isso, o resultado será a saída REAL do neurônio
        return 1 / (1 + math.exp(-total_net_input)) # 1 sobre 1 mais o número de euler (e) elevado à entrada total líquida
                                                    #multiplicada por -1

    def calculate_pd_error_wrt_total_net_input(self, target_output):# Método que determina o quanto a entrada total do neurônio tem
                                                                    #que mudar pra se aproximar da saída esperada. Usa a regra da
                                                                    #cadeia para definir o quanto uma mudança no peso da rede desse
                                                                    #neurônio pesa na saída total
        return self.calculate_pd_error_wrt_output(target_output) * self.calculate_pd_total_net_input_wrt_input();

    def calculate_error(self, target_output): # Calcula o erro de saída do neurônio com a função de erro quadrática
        return 0.5 * (target_output - self.output) ** 2

    def calculate_pd_error_wrt_output(self, target_output): # Método que calcula a derivada parcial do erro com respeito à saída
                                                            #atual(analisa o quanto a saída do neurônio pesa na resposta final)
        return -(target_output - self.output) # Retorna a derivada do erro em função à saída do neurônio.

    def calculate_pd_total_net_input_wrt_input(self): # Método que deriva a saída do neurônio(consigo através da squash) em relação
                                                      #a entrada líquida total dele
        return self.output * (1 - self.output) #Calcula a derivada da saída do neurônio em função da rede

    def calculate_pd_total_net_input_wrt_weight(self, index): # Método que calcula a derivada parcial da entrada líquida total em
                                                              # relação ao peso. Após derivar, o resultado é o valor de entrada do peso
        return self.inputs[index]

class NeuronLayer: # Classe das camadas neurais
    def __init__(self, num_neurons, bias): # Construtor recebe o número de neurônios e o bias da camada
        self.bias = bias if bias else random.random() # Todos os neurônios da camada compartilham do mesmo viés
        self.neurons = [] # Inicializa-se um vetor para guardar os neurônios
        for i in range(num_neurons): # Para o número total de neurônios dessa camada :
            self.neurons.append(Neuron(self.bias)) # Adiciona-se um neurônio ao vetor de neurônios, todos com o mesmo bias
                                                   #e pesos vazios

    def inspect(self): # Imprime os dados da camada
        print('Neurons:', len(self.neurons)) # Número de neurônios
        for n in range(len(self.neurons)): # Repete para cada neurônio
            print(' Neuron', n) # Índice do neurônio no vetor (0-N)

            for w in range(len(self.neurons[n].weights)): # Repete para cada peso relacionado ao neurônio
                print('  Weight:', self.neurons[n].weights[w]) # Imprime todos os pesos das conexões do neurônio
            print('  Bias:', self.bias) # Imprime o bias de cada neurônio da camada (Como só tem um bias para todos, repete)

    def feed_forward(self, inputs): # Método que calcula as saídas de cada neurônio da camada
        outputs = [] # Inicializa vetor para a saída dessa camada
        for neuron in self.neurons: # Repete para cada neurônio
            outputs.append(neuron.calculate_output(inputs)) # Chama o método que calcula a saída para todas as entradas e
                                                            # atribui o valor de saída ao vetor outputs
        return outputs # Retorna vetor contendo a saída de todos os neurônios da camada

    def get_outputs(self): # Método que passa o vetor de saída da camada(chamado após todas as saídas terem sido calculadas)
        outputs = [] # Cria um vetor para receber as saídas
        for neuron in self.neurons: # Repete para cada neurônio da camada
            outputs.append(neuron.output) # Adiciona a saída do neurônio ao vetor
        return outputs # Retorna o vetor preenchido com a saída de cada neurônio

class NeuralNetwork: # Classe da rede neural
    LEARNING_RATE = 0.5 # Taxa de aprendizado

    def __init__(self, num_inputs, num_hidden, num_outputs, hidden_layer_weights = None, hidden_layer_bias = None,
                 output_layer_weights = None, output_layer_bias = None):
        # Construtor da rede, recebe-se número de neurônios da camada escondida e saída, número total de entradas; Vetores com
        #os pesos e bias da camada escondida e, vetores com os pesos dos neurônios da saída e bias.
        self.num_inputs = num_inputs # Inicializa variável com o número de entradas

        self.hidden_layer = NeuronLayer(num_hidden, hidden_layer_bias) # Inicializa nova camada escondida com o número de neurônios e o bias
        self.output_layer = NeuronLayer(num_outputs, output_layer_bias) # Inicializa a camada de saída passando o número de neurônios e o bias

        self.init_weights_from_inputs_to_hidden_layer_neurons(hidden_layer_weights) #Inicializa os pesos das conexões entre a entrada
                                                                                    #e a camada escondida
        self.init_weights_from_hidden_layer_neurons_to_output_layer_neurons(output_layer_weights) # Inicializa pesos das conexões entre
                                                                                                  #a camada escondida e a saída

    def init_weights_from_inputs_to_hidden_layer_neurons(self, hidden_layer_weights): # Método que inicializa os pesos das conexões entrada-c_escondidas
        weight_num = 0 # Contador pra percorrer o vetor de pesos
        for h in range(len(self.hidden_layer.neurons)): # Repete para cada neurônio da camada escondida
            for i in range(self.num_inputs): # Repete para cada entrada
                if not hidden_layer_weights: # Caso o vetor não tenha sido inicializado
                    self.hidden_layer.neurons[h].weights.append(random.random()) # Atribui pesos aleatórios para cada neurônio da camada
                else:
                    self.hidden_layer.neurons[h].weights.append(hidden_layer_weights[weight_num]) # Adiciona pesos do vetor ao neurônio
                weight_num += 1

    def init_weights_from_hidden_layer_neurons_to_output_layer_neurons(self, output_layer_weights): #inicializa os pesos das conexões c_escondida-saída
        weight_num = 0 # Contador pra percorrer o vetor de pesos
        # repetição para fazer o mesmo a todos os neurônios
        for o in range(len(self.output_layer.neurons)): # Repete para cada neurônio da camada de saída
            for h in range(len(self.hidden_layer.neurons)): # Repete para cada neurônio da camada escondida
                if not output_layer_weights: # Caso o vetor não tenha sido inicializado
                    self.output_layer.neurons[o].weights.append(random.random()) # Atribui pesos aleatórios para cada neurônio da camada
                else:
                    self.output_layer.neurons[o].weights.append(output_layer_weights[weight_num]) # Adiciona pesos do vetor ao neurônio
                weight_num += 1

    def inspect(self): # Método que imprime os dados da rede neural
                       # Imprime todas as entradas em um vetor. ENTRADA: a = [8,9,2] SAÍDA: Inputs: [8, 9, 2]
        print('------')
        print('* Inputs: {}'.format(self.num_inputs))
        print('------')
        print('Hidden Layer')
        self.hidden_layer.inspect() # Método que imprime os dados da camada escondida
        print('------')
        print('* Output Layer')
        self.output_layer.inspect() # Método que imprime os dados da camada de saída
        print('------')

    def feed_forward(self, inputs): # Método que realiza a etapa de "ida"
        hidden_layer_outputs = self.hidden_layer.feed_forward(inputs) # Inicializa o vetor que receberá a saída da camada escondida, gerado
                                                                      # pelo método feed_foward da classe de Camada Neural
        return self.output_layer.feed_forward(hidden_layer_outputs) # Repete o processo de feed_foward para a camada de saída
                                                                    #e retorna o vetor contendo a saída final de cada neurônio

    def train(self, training_inputs, training_outputs): # Método que realiza o treino com "Online Searching"(atualiza o peso
                                                        # após cada treinamento)
        self.feed_forward(training_inputs) # Realiza o feed_foward da rede com as entradas
        pd_errors_wrt_output_neuron_total_net_input = [0] * len(self.output_layer.neurons)
        for o in range(len(self.output_layer.neurons)): # Repete para cada neurônios da camada de saída
            pd_errors_wrt_output_neuron_total_net_input[o] = self.output_layer.neurons[o].calculate_pd_error_wrt_total_net_input(training_outputs[o])
            # Chamada do método que realiza a regra da cadeia pra calcular o quanto cada neurônio pesou pra resposta final. O vetor recebe o
            #quanto a mudança em cada peso afetará a saída total

        # Esse bloco de código calcula o quanto uma mudança nos pesos da camada escondida vão afetar a saída total
        pd_errors_wrt_hidden_neuron_total_net_input = [0] * len(self.hidden_layer.neurons)
        for h in range(len(self.hidden_layer.neurons)): # Repete para cada neurônio na camada escondida
            d_error_wrt_hidden_neuron_output = 0 # Inicializa acumulador
            for o in range(len(self.output_layer.neurons)): # Repete para cada neurônio da camada de saída
                d_error_wrt_hidden_neuron_output += pd_errors_wrt_output_neuron_total_net_input[o] * self.output_layer.neurons[o].weights[h]
                # Acumulador recebe o quanto cada peso da camada de saída afeta a saída vezes o peso de cada neurônio
            pd_errors_wrt_hidden_neuron_total_net_input[h] = d_error_wrt_hidden_neuron_output * self.hidden_layer.neurons[h].calculate_pd_total_net_input_wrt_input()
            # Regra da cadeia

        # Esse bloco de código atualiza os pesos da camada de saída
        for o in range(len(self.output_layer.neurons)): # Repete para cada neurônios da camada de saída
            for w_ho in range(len(self.output_layer.neurons[o].weights)): # Repete para cada rede ligadas ao neurônio do loop
                pd_error_wrt_weight = pd_errors_wrt_output_neuron_total_net_input[o] * self.output_layer.neurons[o].calculate_pd_total_net_input_wrt_weight(w_ho)
                # Calcula o resultado final da derivada parcial do erro com respeito aos pesos
                self.output_layer.neurons[o].weights[w_ho] -= self.LEARNING_RATE * pd_error_wrt_weight # Aplica a taxa de aprendizagem aos pesos para atualizar

        # Esse bloco de código atualiza os pesos da camada escondida
        for h in range(len(self.hidden_layer.neurons)): # Repete para cada neurônios da camada escondida
            for w_ih in range(len(self.hidden_layer.neurons[h].weights)):
                pd_error_wrt_weight = pd_errors_wrt_hidden_neuron_total_net_input[h] * self.hidden_layer.neurons[h].calculate_pd_total_net_input_wrt_weight(w_ih)
                # Calcula o resultado final da derivada parcial do erro com respeito aos pesos
                self.hidden_layer.neurons[h].weights[w_ih] -= self.LEARNING_RATE * pd_error_wrt_weight # Aplica a taxa de aprendizagem aos pesos para atualizar

    def calculate_total_error(self, training_sets): # Método que calcula o erro total(somatório de todos os valores de erros dos pesos)
        total_error = 0 # Inicializa acumulador
        for t in range(len(training_sets)): # Repete para cada caso de entrada que houve no treinamento
            training_inputs, training_outputs = training_sets[t] # Atrela a linha do set de treinamento às de entrada e saída
            self.feed_forward(training_inputs) # feed_foward nas entradas de treinamento
            for o in range(len(training_outputs)): # Repete para cada valor de entrada
                total_error += self.output_layer.neurons[o].calculate_error(training_outputs[o]) # Acumula a taxa de erro total com base na taxa de cada peso
        return total_error # Retorna o erro total

# MAIN

nn = NeuralNetwork(2, 2, 2, hidden_layer_weights=[0.15, 0.2, 0.25, 0.3], hidden_layer_bias=0.35, output_layer_weights=[0.4, 0.45, 0.5, 0.55], output_layer_bias=0.6)
for i in range(10000):
    nn.train([0.05, 0.1], [0.01, 0.99])
    print(i, round(nn.calculate_total_error([[[0.05, 0.1], [0.01, 0.99]]]), 9))

# training_sets = [
#     [[0, 0], [0]],
#     [[0, 1], [1]],
#     [[1, 0], [1]],
#     [[1, 1], [0]]
# ]
